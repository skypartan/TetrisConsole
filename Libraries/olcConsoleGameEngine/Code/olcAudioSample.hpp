#pragma once

#include <string>
#include <Windows.h>

class olcAudioSample
{
public:
	olcAudioSample();

	olcAudioSample(std::wstring sWavFile);

	WAVEFORMATEX wavHeader;
	float* fSample = nullptr;
	long nSamples = 0;
	int nChannels = 0;
	bool bSampleValid = false;
};

#define _CRT_SECURE_NO_WARNINGS

#include "game.hpp"
#include "physics.hpp"

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

Game::Tetris::Tetris()
{
	m_sAppName = L"Tetris";

	// Dimensions of field (walls included)
	m_fieldWidth = 12;
	m_fieldHeight = 18;

	m_cornerOffset = 3;
}

Game::Tetris::~Tetris()
{
	delete m_field;
}

bool Game::Tetris::OnUserCreate()
{
	m_state = WELCOME;
	m_field = new short[m_fieldWidth * m_fieldHeight]();

	ResetGame();

	return true;
}

bool Game::Tetris::OnUserUpdate(float elapsedTime)
{
	if (GetKey(27).bPressed) // Escape key
	{
		exit(0);
	}

	ClearScreen();

	switch (m_state)
	{
	case WELCOME:
		ShowWelcomeScreen();
		break;

	case PAUSED:
		ShowPauseScreen();
		break;

	case RUNNING:
		ShowGameScreen();
		break;

	case GAME_OVER:
		ShowGameOverScreen();
		break;
	}

	return true;
}

bool Game::Tetris::OnUserDestroy()
{
	// TODO: Save game history

	return true;
}

void Game::Tetris::ShowWelcomeScreen()
{
	DrawString(0, 0, L"TetrisConsole");
	DrawString(0, 1, L"Made by: skypartan <skypartan@gmail.com>");
	DrawString(0, 2, L"Based on work by OneLoneCoder");

	DrawString(3, 5, L"Press S to start");

	if (GetKey(83).bHeld) // S key pressed
	{
		ClearScreen();

		m_state = RUNNING;
	}
}

void Game::Tetris::ShowGameScreen()
{
	//DrawString(0, 0, to_wstring(elapsedTime));

	// =============== TIMING

	this_thread::sleep_for(50ms); // TODO: Find a better way of doing this

	m_speedCounter++;
	m_fall = (m_speedCounter == m_speed);


	// =============== INPUT

	sKeyState rKey = GetKey(82);
	sKeyState zKey = GetKey(90);

	sKeyState downArrowKey = GetKey(40);
	sKeyState leftArrowKey = GetKey(37);
	sKeyState rightArrowKey = GetKey(39);

	sKeyState pKey = GetKey(80);


	// =============== LOGIC

	if (rKey.bPressed)
	{
		ResetGame();
		m_state = RUNNING;

		return;
	}

	if (pKey.bPressed)
	{
		m_state = PAUSED;

		return;
	}

	// ======== MOVEMENT

	m_currentX -= (leftArrowKey.bHeld // Key is pressed
		&& CheckFit(m_tetromino[m_currentPiece], m_currentRotation, m_currentX - 1, m_currentY, m_field, m_fieldWidth, m_fieldHeight) // Has no collision
		) ? 1 : 0;
	m_currentX += (rightArrowKey.bHeld // Key is pressed
		&& CheckFit(m_tetromino[m_currentPiece], m_currentRotation, m_currentX + 1, m_currentY, m_field, m_fieldWidth, m_fieldHeight) // Has no collision
		) ? 1 : 0;
	m_currentY += (downArrowKey.bHeld // Key is pressed
		&& CheckFit(m_tetromino[m_currentPiece], m_currentRotation, m_currentX, m_currentY + 1, m_field, m_fieldWidth, m_fieldHeight) // Has no collision
		) ? 1 : 0;

	m_currentRotation += (zKey.bPressed // Key is pressed
		&& CheckFit(m_tetromino[m_currentPiece], m_currentRotation + 1, m_currentX, m_currentY, m_field, m_fieldWidth, m_fieldHeight) // Has no collision
		) ? 1 : 0;

	// ======== GRAVITY

	if (m_fall)
	{
		if (CheckFit(m_tetromino[m_currentPiece], m_currentRotation, m_currentX, m_currentY + 1, m_field, m_fieldWidth, m_fieldHeight)) // Piece can fall
		{
			m_currentY++;
		}
		else // Piece has arrived at the end of the field at that position
		{
			// Save piece on field
			for (int x = 0; x < 4; x++)
			{
				for (int y = 0; y < 4; y++)
				{
					if (m_tetromino[m_currentPiece][GetRotatedIndex(x, y, m_currentRotation)] == L'X')
					{
						m_field[(m_currentY + y) * m_fieldWidth + (m_currentX + x)] = m_currentPiece + 1;
					}
				}
			}

			m_pieceCount++;
			if (m_pieceCount % 10 == 0)
				if (m_speed >= 10)
					m_speed--; // If m_speed get smaller, the game gets faster

			// Check for complete lines
			for (int y = 0; y < 4; y++)
			{
				if (m_currentY + y < m_fieldHeight - 1)
				{
					bool line = true;

					for (int x = 1; x < m_fieldWidth - 1; x++) // Remember to consider walls
					{
						//line &= (m_field[(m_currentY + y) * m_fieldWidth + x] != 0);

						line = (m_field[(m_currentY + y) * m_fieldWidth + x] != 0);
						if (!line)
							break;
					}

					if (line)
					{
						for (int x = 1; x < m_fieldWidth - 1; x++) // Remember to consider walls
						{
							m_field[(m_currentY + y) * m_fieldWidth + x] = 8; // 8 => Id of character to represent full line
						}

						m_filledLines.push_back(m_currentY + y);
					}
				}
			}

			m_score += 25;
			if (!m_filledLines.empty())
				m_score += (1 << m_filledLines.size()) * 100;

			// Get new piece
			m_currentPiece = rand() % 7;
			m_currentRotation = 0;
			m_currentX = (m_fieldWidth / 2) - 2; // Start in the center of the field
			m_currentY = 0;

			// Check if game can still run
			if (!CheckFit(m_tetromino[m_currentPiece], m_currentRotation, m_currentX, m_currentY + 1, m_field, m_fieldWidth, m_fieldHeight))
			{
				m_state = GAME_OVER;
			}
		}

		m_speedCounter = 0;
	}

	// =============== DRAWING

	// Draw field
	for (int x = 0; x < m_fieldWidth; x++)
	{
		for (int y = 0; y < m_fieldHeight; y++)
		{
			//Draw(x + m_cornerOffset, y + m_cornerOffset, L" ABCDEFG=#"[m_field[y * m_fieldWidth + x]]/*, m_field[y * m_fieldWidth + x]*/);
			if (m_field[y * m_fieldWidth + x] > 0 && m_field[y * m_fieldWidth + x] < 8)
			{
				Draw(x + m_cornerOffset, y + m_cornerOffset, 9608, m_tetromino_colors[m_field[y * m_fieldWidth + x] - 1]);
			}
			else
			{
				Draw(x + m_cornerOffset, y + m_cornerOffset, L" ABCDEFG=#"[m_field[y * m_fieldWidth + x]], 8);
			}
		}
	}

	// Draw current piece
	for (int x = 0; x < 4; x++)
	{
		for (int y = 0; y < 4; y++)
		{
			if (m_tetromino[m_currentPiece][GetRotatedIndex(x, y, m_currentRotation)] == L'X')
			{
				//Draw((m_currentX + x + m_cornerOffset), (m_currentY + y + m_cornerOffset), L"ABCDEFG"[m_currentPiece], m_currentPiece + 1);
				Draw((m_currentX + x + m_cornerOffset), (m_currentY + y + m_cornerOffset), 9608, m_tetromino_colors[m_currentPiece]);
			}
		}
	}

	// Draw score
	DrawString((m_cornerOffset + m_fieldWidth + 5), 16, L"Score: " + to_wstring(m_score));

	// Remove filled lines
	if (!m_filledLines.empty())
	{
		//this_thread::sleep_for(500ms); // Gambiaraaaaaaaaaaaaaaa 2

		for (int& line : m_filledLines)
		{
			for (int x = 1; x < m_fieldWidth - 1; x++)
			{
				for (int y = line; y > 0; y--)
				{
					m_field[y * m_fieldWidth + x] = m_field[(y - 1) * m_fieldWidth + x];
				}

				m_field[x] = 0;
			}
		}

		m_filledLines.clear();
	}
}

void Game::Tetris::ShowPauseScreen()
{
	if (GetKey(80).bPressed)
	{
		m_state = RUNNING;
	}

	DrawString(10, 10, L"Paused");
}

void Game::Tetris::ShowGameOverScreen()
{
	sKeyState rKey = GetKey(82);

	if (rKey.bPressed)
	{
		ResetGame();
		m_state = RUNNING;

		return;
	}

	DrawString(0, 0, L"Game Over");
	DrawString(10, 10, L"Score: " + to_wstring(m_score));

	for (int i = 0; i < 5; i++)
	{
		DrawString(25, 2, to_wstring(history[i]));
	}
}

void Game::Tetris::ClearScreen()
{
	Fill(0, 0, 60, 30, ' ', 0x00); // Clear screen

	DrawString(0, 28, L"Z - Rotate; Arrow keys - Movement; R - Reset; Esc - Quit");
	DrawString(0, 29, L"P - Pause");
}

void Game::Tetris::ResetGame()
{
	for (int x = 0; x < m_fieldWidth; x++)
	{
		for (int y = 0; y < m_fieldHeight; y++)
		{
			// 0 -> Blank space
			// 9 -> Wall
			int cell = (x == 0 || x == m_fieldWidth - 1 || y == m_fieldHeight - 1) ? 9 : 0;
			m_field[y * m_fieldWidth + x] = cell;
		}
	}

	m_currentPiece = rand() % 7;
	m_currentRotation = 0;
	m_currentX = (m_fieldWidth / 2) - 2; // Start in the center of the field
	m_currentY = 0;

	m_speed = 20;
	m_speedCounter = 0;
	m_fall = false;
	m_gameOver = false;

	m_pieceCount = 0;
	m_score = 0;
}
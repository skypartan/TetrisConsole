#pragma once

#include <iostream>
#include <olcConsoleGameEngine.hpp>

namespace Game
{
	enum GameState
	{
		WELCOME,
		RUNNING,
		PAUSED,
		GAME_OVER
	};

	class Tetris : public olcConsoleGameEngine
	{
	private:
		std::wstring m_tetromino[7] = {
			// I
			L"..X."
			L"..X."
			L"..X."
			L"..X.",
			// Z
			L"..X."
			L".XX."
			L".X.."
			L"....",
			// S
			L".X.."
			L".XX."
			L"..X."
			L"....",
			// O
			L"...."
			L".XX."
			L".XX."
			L"....",
			// T
			L"..X."
			L".XX."
			L"..X."
			L"....",
			// L
			L"...."
			L".XX."
			L"..X."
			L"..X.",
			// J
			L"...."
			L".XX."
			L".X.."
			L".X..",
		};
		int m_tetromino_colors[7] = { 1, 2, 3, 4, 5, 6, 7 };

		int m_fieldWidth;
		int m_fieldHeight;

		short* m_field;
		std::vector<int> m_filledLines;
		int m_pieceCount;
		int m_score;

		int m_cornerOffset;

		int m_currentPiece;
		int m_currentRotation;
		int m_currentX;
		int m_currentY;

		int m_speed;
		int m_speedCounter;
		bool m_fall;
		bool m_gameOver;

		GameState m_state;

		int history[5];

	public:
		Tetris();
		~Tetris();

		bool OnUserCreate() override;
		bool OnUserUpdate(float fElapsedTime) override;
		bool OnUserDestroy() override;

	public:
		void ShowWelcomeScreen();
		void ShowGameScreen();
		void ShowPauseScreen();
		void ShowGameOverScreen();

		void ClearScreen();
		void ResetGame();
	};
}

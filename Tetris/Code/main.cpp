#include "game.hpp"

int main()
{
	srand(time(0));

	Game::Tetris game;
	if (game.ConstructConsole(60, 30, 14, 14))
	{
		game.Start();
	}

	return EXIT_SUCCESS;
}

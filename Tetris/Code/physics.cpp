#include "physics.hpp"

#include <string>

using namespace std;

/*
 * Returns the index of (px, py) inside piece array relative to rotation
 */
int Game::GetRotatedIndex(int px, int py, int rotation)
{
	switch (rotation % 4)
	{
	case 0:
		return (py * 4 + px);			//   0�
	case 1:
		return (12 + py - (px * 4));	//  90�
	case 2:
		return (15 - (py * 4) - px);	// 180�
	case 3:
		return (3 - py + (px * 4));		// 270�
	}

	return 0;
}

/*
 * px - Piece matrix X position
 * py - Piece matrix Y position
 */
bool Game::CheckFit(wstring piece, int rotation, int px, int py, short* field, int fieldWidth, int fieldHeight)
{
	for (int x = 0; x < 4; x++)
	{
		for (int y = 0; y < 4; y++)
		{
			int piece_index = GetRotatedIndex(x, y, rotation); // Get index into piece
			int field_index = (py + y) * fieldWidth + (px + x); // Get index into field

			if ((px + x >= 0 && px + x < fieldWidth) && (py + y >= 0 && py + y < fieldHeight))
			{
				if (piece[piece_index] == L'X' && field[field_index] != 0)
					return false; // Fails on first hit
			}
		}
	}

	return true;
}
#pragma once

#include <string>

namespace Game
{
	int GetRotatedIndex(int px, int py, int rotation);
	bool CheckFit(std::wstring piece, int rotation, int px, int py, short* field, int fieldWidth, int fieldHeight);
}

#pragma once

#include <fstream>

namespace Game
{
	struct GameData
	{
		int* scoreHistory;
	};

	void Store(GameData data);
	GameData Load();
}